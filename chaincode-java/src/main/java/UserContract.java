import java.util.ArrayList;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contact;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.License;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import com.owlike.genson.Genson;

@Contract(
        name = "user",
        info = @Info(
                title = "User Contract",
                description = "",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "",
                        name = "",
                        url = "https://hyperledger.example.com")))
@Default
public class UserContract implements ContractInterface {

    private final Genson genson = new Genson();

    private enum UserErrors {
        User_NOT_FOUND,
        User_ALREADY_EXISTS,
        INCOMPLETE_INPUT,
        DATA_ERROR
    }
    /**
     * Creates a new User on the ledger.
     *
     * @param ctx the transaction context
     * @param userId
     * @param name
     * @param isDriver
     * @return the created User
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public User createUser(final Context ctx, final String userId, final String name, final Ride userRide, final boolean isDriver) {
        ChaincodeStub stub = ctx.getStub();

        //input validations
        String errorMessage = null;
        if (userId == null || userId.equals("")) {
            errorMessage = String.format("Empty input: userId");
        }

        if (errorMessage != null) {
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, UserErrors.INCOMPLETE_INPUT.toString());
        }
        // Check if user already exists
        byte[] userJSON = ctx.getStub().getState(userId);
        if (userJSON != null && userJSON.length > 0) {
            errorMessage = String.format("User %s already exists", userId);
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, UserErrors.User_ALREADY_EXISTS.toString());
        }

        User user = new User(userId, name, userRide, isDriver);


        userJSON = user.serialize();
        System.out.printf("CreateUser Put: userId %s Data %s\n", userId, new String(userJSON));

        stub.putState(userId, userJSON);
        // add Event data to the transaction data. Event will be published after the block containing
        // this transaction is committed
        stub.setEvent("createUser", userJSON);
        return user;
    }

    /**
     * Deletes user on the ledger.
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void deleteUser(final Context ctx, final String userId) {
        ChaincodeStub stub = ctx.getStub();

        System.out.printf("DeleteUser: verify asset %s exists\n", userId);
        User user = getState(ctx, userId);

        System.out.printf(" DeleteUser:  ID %s\n", userId);

        stub.delState(userId);         // delete the key from Statedb
        stub.setEvent("deleteUser", user.serialize()); //publish Event
    }

    /**
     * Retrieves a User with the specified Id from the ledger.
     * @return the user found on the ledger if there was one
     */
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String getUser(final Context ctx, final String userId) {
        System.out.printf("GetUser: userId %s\n", userId);

        User user = getState(ctx, userId);
        return user.toString();
    }

    /**
     * Retrieves all users from the ledger.
     * @return array of users found on the ledger
     */
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String GetAllUsers(final Context ctx) {
        ChaincodeStub stub = ctx.getStub();

        ArrayList<User> queryResults = new ArrayList<User>();

        // To retrieve all users from the ledger use getStateByRange with empty startKey & endKey.
        // Giving empty startKey & endKey is interpreted as all the keys from beginning to end.
        // As another example, if you use startKey = 'user0', endKey = 'user9' ,
        // then getStateByRange will retrieve user with keys between user0 (inclusive) and user9 (exclusive) in lexical order.
        QueryResultsIterator<KeyValue> results = stub.getStateByRange("", "");

        for (KeyValue result: results) {
            User user = genson.deserialize(result.getStringValue(), User.class);
            System.out.println(user);
            queryResults.add(user);
        }

        final String response = genson.serialize(queryResults);

        return response;
    }

    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public User UpdateUser(final Context ctx, final String userId, final String name, final Ride userRide, final boolean isDriver) {
        ChaincodeStub stub = ctx.getStub();
        //input validations
        String errorMessage = null;
        if (userId == null || userId.equals("")) {
            errorMessage = String.format("Empty input: userID");
        }

        if (errorMessage != null) {
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, UserErrors.INCOMPLETE_INPUT.toString());
        }
        // reads from the Statedb. Check if asset already exists
        User user = getState(ctx, userId);

        if (name != null) {
            user.setName(name);
        }
        if (userRide != null) {
            user.setUserRide(userRide);
        }
        user.isDriver(isDriver);

        byte[] userJSON = user.serialize();
        System.out.printf("UpdateUser Put: userId %s Data %s\n", userId, new String(userJSON));
        stub.putState(userId, userJSON);
        stub.setEvent("updateUser", userJSON); //publish Event
        return user;
    }

    private User getState(final Context ctx, final String userId) {
        byte[] userJSON = ctx.getStub().getState(userId);
        if (userJSON == null || userJSON.length == 0) {
            String errorMessage = String.format("User %s does not exist", userId);
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, UserContract.UserErrors.User_NOT_FOUND.toString());
        }

        try {
            User user = User.deserialize(userJSON);
            return user;
        } catch (Exception e) {
            throw new ChaincodeException("Deserialize error: " + e.getMessage(), UserContract.UserErrors.DATA_ERROR.toString());
        }
    }
}
