import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contact;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.License;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import com.owlike.genson.Genson;

@Contract(
        name = "fyp",
        info = @Info(
                title = "Ridehail Contract",
                description = "The hyperlegendary asset transfer events sample",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "a.transfer@example.com",
                        name = "Fabric Development Team",
                        url = "https://hyperledger.example.com")))
@Default
public final class RidehailContract implements ContractInterface {

    private final Genson genson = new Genson();

    private enum RideHailErrors {
        Ride_NOT_FOUND,
        Ride_ALREADY_EXISTS,
        INCOMPLETE_INPUT,
        DATA_ERROR
    }

    /**
     * Creates some initial rides on the ledger.
     *
     * @param ctx the transaction context
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void InitLedger(final Context ctx) {
        ChaincodeStub stub = ctx.getStub();

        CreateRide(ctx, "ride1", "user21", "user51", "St. Patrick's Cathedral", "Dublin Castle", LocalDateTime.of(2022, Month.MAY, 20, 13, 5, 55), LocalDateTime.of(2022, Month.MAY, 20, 13, 20, 13), "completed");
        CreateRide(ctx, "ride2", "user39", "user87", "UCD", "Trinity College", LocalDateTime.of(2022, Month.JUNE, 9, 8, 30, 3), LocalDateTime.of(2022, Month.JUNE, 9, 9, 5, 58), "completed");
        CreateRide(ctx, "ride3", "user74", "user62", "Jervis Shopping Centre", "Temple Bar", LocalDateTime.of(2022, Month.JULY, 1, 15, 20, 45), LocalDateTime.of(2022, Month.JULY, 1, 15, 35, 6), "completed");

    }

    /**
     * Creates a new Ride on the ledger.
     *
     * @param ctx the transaction context
     * @param rideId the ID of the ride
     * @param riderId the ID of the rider
     * @param driverId the ID of the driver
     * @param pickUpLocation the origin of the ride
     * @param dropOffLocation the destination for the ride
     * @param departureTime the starting time of the ride
     * @param arrivalTime the finishing time of the ride
     * @param status the status of the ride
     * @return the created Ride
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public Ride CreateRide(final Context ctx, final String rideId, final String riderId, final String driverId,
                           final String pickUpLocation, final String dropOffLocation,
                           final LocalDateTime departureTime, final LocalDateTime arrivalTime, final String status) {
        ChaincodeStub stub = ctx.getStub();

        //input validations
        String errorMessage = null;
        if (rideId == null || rideId.equals("")) {
            errorMessage = String.format("Empty input: rideId");
        }

        if (errorMessage != null) {
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, RideHailErrors.INCOMPLETE_INPUT.toString());
        }

        // Check if ride already exists
        byte[] rideJSON = ctx.getStub().getState(rideId);
        if (rideJSON != null && rideJSON.length > 0) {
            errorMessage = String.format("Ride %s already exists", rideId);
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, RideHailErrors.Ride_ALREADY_EXISTS.toString());
        }

        Ride ride = new Ride(rideId, riderId,  driverId, pickUpLocation, dropOffLocation, departureTime, arrivalTime, status);

        rideJSON = ride.serialize();
        System.out.printf("CreateRide Put: rideId %s Data %s\n", rideId, new String(rideJSON));

        stub.putState(rideId, rideJSON);
        // add Event data to the transaction data. Event will be published after the block containing
        // this transaction is committed
        stub.setEvent("CreateRide", rideJSON);
        return ride;
    }

    /**
     * Deletes ride on the ledger.
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void DeleteRide(final Context ctx, final String rideId) {
        ChaincodeStub stub = ctx.getStub();
        System.out.printf("DeleteRide: verify ride %s exists\n", rideId);
        Ride ride = getState(ctx, rideId);

        System.out.printf(" DeleteRide:  rideId %s\n", rideId);

        stub.delState(rideId);         // delete the key from Statedb
        stub.setEvent("DeleteRide", ride.serialize()); //publish Event
    }

    /**
     * Retrieves a Ride with the specified Id from the ledger.
     * @return the ride found on the ledger if there was one
     */
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String GetRide(final Context ctx, final String rideId) {
        System.out.printf("GetRide: rideId %s\n", rideId);

        Ride ride = getState(ctx, rideId);
        return ride.toString();
    }

    /**
     * Retrieves all rides from the ledger.
     * @return array of rides found on the ledger
     */
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String GetAllRides(final Context ctx) {
        ChaincodeStub stub = ctx.getStub();

        ArrayList<Ride> queryResults = new ArrayList<Ride>();

        // To retrieve all rides from the ledger use getStateByRange with empty startKey & endKey.
        // Giving empty startKey & endKey is interpreted as all the keys from beginning to end.
        // As another example, if you use startKey = 'ride0', endKey = 'ride9' ,
        // then getStateByRange will retrieve ride with keys between ride0 (inclusive) and ride9 (exclusive) in lexical order.
        QueryResultsIterator<KeyValue> results = stub.getStateByRange("", "");

        for (KeyValue result: results) {
            Ride ride = genson.deserialize(result.getStringValue(), Ride.class);
            System.out.println(ride);
            queryResults.add(ride);
        }

        final String response = genson.serialize(queryResults);

        return response;
    }

    /**
     * Updates the properties of a Ride on the ledger.
     * @return the updated Ride
     */
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public Ride UpdateRide(final Context ctx, final String rideId, final String riderId, final String driverId,
                           final String pickUpLocation, final String dropOffLocation,
                           final LocalDateTime departureTime, final LocalDateTime arrivalTime, final String status) {
        ChaincodeStub stub = ctx.getStub();

        //input validations
        String errorMessage = null;
        if (rideId == null || rideId.equals("")) {
            errorMessage = String.format("Empty input: rideId");
        }

        if (errorMessage != null) {
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, RideHailErrors.INCOMPLETE_INPUT.toString());
        }

        // reads from the Statedb. Check if ride already exists
        Ride ride = getState(ctx, rideId);

        if (riderId != null) {
            ride.setRiderId(riderId);
        }
        if (driverId != null) {
            ride.setDriverId(driverId);
        }
        if (pickUpLocation != null) {
            ride.setPickUpLocation(pickUpLocation);
        }
        if (dropOffLocation != null) {
            ride.setDropOffLocation(dropOffLocation);
        }
        if (departureTime != null) {
            ride.setDepartureTime(departureTime);
        }
        if (arrivalTime != null) {
            ride.setArrivalTime(arrivalTime);
        }
        if (status != null) {
            ride.setStatus(status);
        }

        byte[] rideJSON = ride.serialize();
        System.out.printf("UpdateRide Put: rideId %s Data %s\n", rideId, new String(rideJSON));
        stub.putState(rideId, rideJSON);
        stub.setEvent("UpdateRide", rideJSON); //publish Event
        return ride;
    }

    private Ride getState(final Context ctx, final String rideId) {
        byte[] rideJSON = ctx.getStub().getState(rideId);
        if (rideJSON == null || rideJSON.length == 0) {
            String errorMessage = String.format("Ride %s does not exist", rideId);
            System.err.println(errorMessage);
            throw new ChaincodeException(errorMessage, RideHailErrors.Ride_NOT_FOUND.toString());
        }

        try {
            Ride ride = Ride.deserialize(rideJSON);
            return ride;
        } catch (Exception e) {
            throw new ChaincodeException("Deserialize error: " + e.getMessage(), RideHailErrors.DATA_ERROR.toString());
        }
    }
}
