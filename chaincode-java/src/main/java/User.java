import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

@DataType
public final class User {
    @Property
    private final String userId;

    @Property
    private String name;

    @Property
    private Ride userRide;

    @Property
    private boolean isDriver;

    public User(final String userId, final String name, final Ride userRide, final boolean isDriver) {

        this.userId = userId;
        this.name = name;
        this.userRide = userRide;
        this.isDriver = isDriver;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public Ride getUserRide() {
        return userRide;
    }

    public boolean isDriver() {
        return isDriver;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setUserRide(final Ride userRide) {
        this.userRide = userRide;
    }

    public void isDriver(final boolean isDriver) {
        this.isDriver = isDriver;
    }

    public byte[] serialize() {
        Map<String, Object> tMap = new HashMap();
        tMap.put("userId", userId);
        tMap.put("name",  name);
        tMap.put("userRide", userRide.toString());
        tMap.put("isDriver", Boolean.toString(isDriver));

        return new JSONObject(tMap).toString().getBytes(UTF_8);
    }

    public static User deserialize(final byte[] uJSON) {
        final String userJSON = new String(uJSON, UTF_8);

        JSONObject json = new JSONObject(userJSON);
        Map<String, Object> tMap = json.toMap();

        final String userId = (String) tMap.get("userId");
        final String name = (String) tMap.get("name");
        final boolean isDriver = Boolean.parseBoolean((String) tMap.get("isDriver"));
        Ride userRide = Ride.parse((String) tMap.get("userRide"));

        return new User(userId, name, userRide, isDriver);

    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getName(), getUserRide(), isDriver());
    }

    @Override
    public String toString() {
        return "userId=" + userId + ", name=" + name + ", userRide=" +
                userRide.toString() + ", isDriver=" + isDriver;
    }
}
