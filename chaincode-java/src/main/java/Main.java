import java.time.LocalDateTime;
import java.time.Month;

public class Main {
    public static void main(String[] args) {

        User user = new User("User51", "Emanuel", new Ride(null, null, null,
                null, null, null, null, null), false);
        Ride ride = new Ride("089", "User123", "User543", "St.Patrick's Cathedral",
                "Dublin Castle", LocalDateTime.of(2022, Month.MAY, 20, 13, 05, 55),
                LocalDateTime.of(2022, Month.MAY, 20, 13, 20, 01), "completed");

        user.setUserRide(ride);

        System.out.println("Ride:\n" + ride);
        System.out.println("\nUser:\n" + user);

        System.out.println("\nRide:\n" + ride.toString());
        System.out.println("\nUser:\n" + user.toString());

        byte[] rideJson = ride.serialize();
        byte[] userJson = user.serialize();

        System.out.println("\nSerialized Ride:\n" + rideJson);
        System.out.println("\nSerialized User:\n" + userJson);

        User deserializedUser = User.deserialize(userJson);
        Ride deserializedRide = Ride.deserialize(rideJson);

        System.out.println("\nDeserialized Ride:\n" + deserializedRide);
        System.out.println("\nDeserialized User:\n" + deserializedUser);
    }
}

