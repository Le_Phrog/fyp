import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

@DataType()
public final class Ride {

    @Property()
    private final String rideId;

    @Property()
    private String riderId;

    @Property()
    private String driverId;
    
    @Property()
    private String pickUpLocation;

    @Property()
    private String dropOffLocation;

    @Property()
    private LocalDateTime departureTime;

    @Property()
    private LocalDateTime arrivalTime;

    @Property()
    private String status;


    public String getRideId() {
        return rideId;
    }

    public String getRiderId() {
        return riderId;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public String getStatus() {
        return status;
    }

    public void setRiderId(final String riderId) {
        this.riderId = riderId;
    }

    public void setDriverId(final String driverId) {
        this.driverId = driverId;
    }

    public void setPickUpLocation(final String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public void setDropOffLocation(final String dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public void setDepartureTime(final LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public void setArrivalTime(final LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public Ride(final String rideId, final String riderId, final String driverId,
                final String pickUpLocation, final String dropOffLocation, final LocalDateTime departureTime,
                final LocalDateTime arrivalTime, final String status) {
        this.rideId = rideId;
        this.riderId = riderId;
        this.driverId = driverId;
        this.pickUpLocation = pickUpLocation;
        this.dropOffLocation = dropOffLocation;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.status = status;
    }

    public byte[] serialize() {
        Map<String, Object> tMap = new HashMap();
        tMap.put("rideId", rideId);
        tMap.put("riderId", riderId);
        tMap.put("driverId", driverId);
        tMap.put("pickUpLocation", pickUpLocation);
        tMap.put("dropOffLocation", dropOffLocation);
        tMap.put("departureTime", departureTime.toString());
        tMap.put("arrivalTime", arrivalTime.toString());
        tMap.put("status", status);

        return new JSONObject(tMap).toString().getBytes(UTF_8);
    }

    public static Ride deserialize(final byte[] rJSON) {
        final String rideJSON = new String(rJSON, UTF_8);

        JSONObject json = new JSONObject(rideJSON);
        Map<String, Object> tMap = json.toMap();

        return getRideMapData(tMap);
    }

    private static Ride getRideMapData(Map<String, Object> attributeMap) {
        final String rideId = (String) attributeMap.get("rideId");
        final String riderId = (String) attributeMap.get("riderId");
        final String driverId = (String) attributeMap.get("driverId");
        final String pickUpLocation = (String) attributeMap.get("pickUpLocation");
        final String dropOffLocation = (String) attributeMap.get("dropOffLocation");
        final String status = (String) attributeMap.get("status");

        LocalDateTime departureTime = LocalDateTime.now();
        LocalDateTime arrivalTime = LocalDateTime.now();


        if (attributeMap.containsKey("departureTime")) {
            departureTime = LocalDateTime.parse((String) attributeMap.get("departureTime"));
        }
        if (attributeMap.containsKey("arrivalTime")) {
            arrivalTime = LocalDateTime.parse((String) attributeMap.get("arrivalTime"));
        }
        return new Ride(rideId, riderId, driverId, pickUpLocation, dropOffLocation, departureTime, arrivalTime, status);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Ride otherRide = (Ride) obj;

        return Objects.deepEquals(
                new String[] {getRideId(), getRiderId(), getDriverId(), getPickUpLocation(), getDropOffLocation()},
                new String[] {otherRide.getRideId(), otherRide.getRiderId(), otherRide.getDriverId(),
                        otherRide.getPickUpLocation(), otherRide.getDropOffLocation()})
                &&
                Objects.deepEquals(
                        new LocalDateTime[] {getDepartureTime(), getArrivalTime()},
                        new LocalDateTime[] {otherRide.getDepartureTime(), otherRide.getArrivalTime()});
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRideId(), getRiderId(), getDriverId(), getPickUpLocation(), getDropOffLocation(),
                getDepartureTime(), getArrivalTime(), getStatus());
    }

    @Override
    public String toString() {
        return "rideId=" + rideId + ", riderId=" + riderId + ", driverId=" + driverId +
                ", pickUpLocation=" + pickUpLocation + ", dropOffLocation=" + dropOffLocation +
                ", departureTime=" + departureTime.toString() + ", arrivalTime=" + arrivalTime.toString() +
                ", status=" + status;
    }

    public static Ride parse(final String rideData) {
        String[] rideDataAttributes = rideData.split(", ");
        HashMap<String, Object> attributeMap = new HashMap<String, Object>();
        for (String attribute : rideDataAttributes) {
            String[] ridePair = attribute.split("=");
            attributeMap.put(ridePair[0], ridePair[1]);
        }

        Ride ride = getRideMapData(attributeMap);
        return ride;
    }
}
