import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.grpc.ManagedChannel;
import org.hyperledger.fabric.client.CallOption;
import org.hyperledger.fabric.client.ChaincodeEvent;
import org.hyperledger.fabric.client.CloseableIterator;
import org.hyperledger.fabric.client.CommitException;
import org.hyperledger.fabric.client.CommitStatusException;
import org.hyperledger.fabric.client.Contract;
import org.hyperledger.fabric.client.EndorseException;
import org.hyperledger.fabric.client.Gateway;
import org.hyperledger.fabric.client.Network;
import org.hyperledger.fabric.client.Status;
import org.hyperledger.fabric.client.SubmitException;
import org.hyperledger.fabric.client.SubmittedTransaction;

public final class App {
    private static final String channelName = "mychannel";
    //private static final String userChaincodeName = "user";
    private static final String rideChaincodeName = "fyp";

    private final Network network;
    //private final Contract userContract;
    private final Contract rideContract;

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    //
    private final String rideId = "ride" + Instant.now().toEpochMilli();
    private final String driverId = "user1";
    private final String riderId = "user2";
    private LocalDateTime departureTime = null;

    //user details
//    private String name = null;
//    private boolean isDriver = false;

    public static void main(final String[] args) throws Exception {
        ManagedChannel grpcChannel = Connections.newGrpcConnection();
        Gateway.Builder builder = Gateway.newInstance()
                .identity(Connections.newIdentity())
                .signer(Connections.newSigner())
                .connection(grpcChannel)
                .evaluateOptions(CallOption.deadlineAfter(5, TimeUnit.SECONDS))
                .endorseOptions(CallOption.deadlineAfter(15, TimeUnit.SECONDS))
                .submitOptions(CallOption.deadlineAfter(5, TimeUnit.SECONDS))
                .commitStatusOptions(CallOption.deadlineAfter(1, TimeUnit.MINUTES));

        try (Gateway gateway = builder.connect()) {
            new App(gateway).run();
        } finally {
            grpcChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }

    public App(final Gateway gateway) {
        network = gateway.getNetwork(channelName);
        rideContract = network.getContract(rideChaincodeName);
        //userContract = network.getContract(userChaincodeName);
    }

    public void run() throws EndorseException, SubmitException, CommitStatusException, CommitException {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("\n*** Enter your name");
//        name = scanner.nextLine();
//
//        System.out.println("\n*** Enter 'y' if you're a driver, 'n' if you're a passenger: ");
//        while (!scanner.hasNext("[yYnN]")) {
//            System.out.println("\n*** Invalid Input");
//            System.out.println("\n*** Enter 'y' if you're a driver, 'n' if you're a passenger: ");
//            scanner.next();
//        }
//        String str = scanner.next();
//
//        boolean isDriver = false;
//        if (str.equals("y") || str.equals("Y")) {
//            isDriver = true;
//        } else {
//            isDriver = false;
//        }

        // Listen for events emitted by subsequent transactions, stopping when the try-with-resources block exits
        try (CloseableIterator<ChaincodeEvent> eventSession = startChaincodeEventListening()) {
            requestRide();
            acceptRide();
            pickUpRider();
            dropOffRider();
        }
    }

    private CloseableIterator<ChaincodeEvent> startChaincodeEventListening() {
        System.out.println("\n*** Start chaincode event listening");

        CloseableIterator<ChaincodeEvent> eventIter = network.getChaincodeEvents(rideChaincodeName);

        CompletableFuture.runAsync(() -> {
            eventIter.forEachRemaining(event -> {
                String payload = prettyJson(event.getPayload());
                System.out.println("\n<-- Chaincode event received: " + event.getEventName() + " - " + payload);
            });
        });

        return eventIter;
    }

    private String prettyJson(final byte[] json) {
        return prettyJson(new String(json, StandardCharsets.UTF_8));
    }

    private String prettyJson(final String json) {
        JsonElement parsedJson = JsonParser.parseString(json);
        return gson.toJson(parsedJson);
    }

    // create user (rider/driver) sets id, name, isDriver
//    private void createUser(String userId, Ride userRide, boolean isDriver) {
//
//    }

    // delete user
//    private void deleteUser(String userId) {
//
//    }


    // create ride (rider) sets the rider Id, starting location, and status to requested
    private void requestRide() throws EndorseException, SubmitException, CommitStatusException {
        System.out.println("\n--> Submit transaction: RequestRide, " + rideId);

        SubmittedTransaction commit = rideContract.newProposal("CreateRide")
                .addArguments(rideId, riderId, "Driver not decided", "St.Patrick's Cathedral", "Dublin Castle", LocalDateTime.now().toString(), LocalDateTime.now().toString(), "requested")
                .build()
		.endorse()
                .submitAsync();

        Status status = commit.getStatus();
        if (!status.isSuccessful()) {
            throw new RuntimeException("failed to commit transaction with status code " + status.getCode());
        }

        System.out.println("\n*** RequestRide committed successfully");
    }

    // update ride (driver) sets the driver Id and status to enroute
    private void acceptRide() throws EndorseException, SubmitException, CommitStatusException, CommitException {
        System.out.println("\n--> Submit transaction: AcceptRide, " + rideId);

        rideContract.submitTransaction("UpdateRide", rideId, riderId, driverId, "St.Patrick's Cathedral", "Dublin Castle", LocalDateTime.now().toString(), LocalDateTime.now().toString(), "enroute");

        System.out.println("\n*** AcceptRide committed successfully");
    }

    // updateRide (rider) changes the status parameter to started and sets the pickUpTime
    private void pickUpRider() throws EndorseException, SubmitException, CommitStatusException, CommitException {
        System.out.println("\n--> Submit transaction: PickUpRider");

        departureTime = LocalDateTime.now();
        rideContract.submitTransaction("UpdateRide", rideId, riderId, driverId, "St.Patrick's Cathedral",
                "Dublin Castle", departureTime.toString(), LocalDateTime.now().toString(), "started");

        System.out.println("\n*** PickUpRider committed successfully");
    }

    // updateRide (driver) changes the status parameter to completed and sets the dropOffTime
    private void dropOffRider() throws EndorseException, SubmitException, CommitStatusException, CommitException{
        System.out.println("\n--> Submit transaction: DropOffRider");

        rideContract.submitTransaction("UpdateRide", rideId, riderId, driverId, "St.Patrick's Cathedral",
                "Dublin Castle", departureTime.toString(), LocalDateTime.now().toString(), "completed");

        System.out.println("\n*** DropOffRider committed successfully");
    }
}
